package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

// Userテーブル用のDao
public class UserDao {

  // ログインIDとパスワードに紐づくユーザ情報を返す
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      boolean isAdmin = rs.getBoolean("is_admin");
      return new User(loginIdData, nameData, isAdmin);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // 全てのユーザ情報を返す
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);


      // 結果表に格納されたレコードの内容をArrayListインスタンスに追加（データベースから取り出す際はこれらの型を使う）
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
        userList.add(user);
      }
      return userList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // 新規ユーザー登録（作成されたユーザのloginIdを返す）
  public void createUser(String loginId, String name, String birthDate, String password,
      String createDate, String updateDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO user (login_id, name, birth_date, password, is_admin, create_date, update_date)"
              + "VALUES (?, ?, ?, ?, false, ?, ?)";

      // INSERTを実行し、ユーザーデータを生成
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      pStmt.setString(5, createDate);
      pStmt.setString(6, updateDate);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // IDに紐づくユーザ情報を返す
  public User findById(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

      return user;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void updateUser(String name, String birthDate, String password, String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      if (password == null) {
        String sql = "UPDATE user SET name = ?, birth_date = ? WHERE login_id = ?";
        // UPDATEを実行し、ユーザーデータを更新
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, name);
        pStmt.setString(2, birthDate);
        pStmt.setString(3, loginId);
        pStmt.executeUpdate();
      } else {
        String sql = "UPDATE user SET name = ?, birth_date = ?, password = ? WHERE login_id = ?";
        // UPDATEを実行し、ユーザーデータを更新
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, name);
        pStmt.setString(2, birthDate);
        pStmt.setString(3, password);
        pStmt.setString(4, loginId);
        pStmt.executeUpdate();
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // IDに紐づくユーザ情報を削除
  public void deleteUser(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "DELETE FROM user WHERE login_id = ?";
      // DELETE文を実行し、ユーザーデータを削除
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // 検索結果に紐づくユーザ情報を返す
  public List<User> findUser(String loginId, String name, String startBirthDate,
      String endBirthDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE 1=1 ";

      // 文字を連結させる(sql+)
      StringBuilder stringBuilder = new StringBuilder(sql);

      List<String> parameters = new ArrayList<>();

      if (!loginId.isEmpty()) {
        stringBuilder.append("AND login_id = ? ");
        parameters.add("loginId");
      }
      if (!name.isEmpty()) {
        stringBuilder.append("AND name LIKE ? ");
        parameters.add("name");
      }
      if (!startBirthDate.isEmpty()) {
        stringBuilder.append("AND birth_date >= ? ");
        parameters.add("startBirthDate");
      }
      if (!endBirthDate.isEmpty()) {
        stringBuilder.append("AND birth_date <= ? ");
        parameters.add("endBirthDate");
      }

      // SELECT文を実行し、ユーザーデータを入手
      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
      int columnIndex = 0;

      if (parameters.contains("loginId")) {
        pStmt.setString(++columnIndex, loginId);
      }
      if (parameters.contains("name")) {
        pStmt.setString(++columnIndex, "%" + name + "%");
      }
      if (parameters.contains("startBirthDate")) {
        pStmt.setString(++columnIndex, startBirthDate);
      }
      if (parameters.contains("endBirthDate")) {
        pStmt.setString(++columnIndex, endBirthDate);
      }

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId2 = rs.getString("login_id");
        String name2 = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId2, name2, birthDate, password, isAdmin, createDate, updateDate);
        userList.add(user);
      }
      return userList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

}


