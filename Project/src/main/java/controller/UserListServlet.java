package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserListServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // TODO：ログインセッションがない場合、ログイン画面にリダイレクトさせる
    // セッションからログインユーザの情報をセット
    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {
      // ログインサーブレットへリダイレクト
      response.sendRedirect("LoginServlet");
      return;
    }


    // ユーザ一覧情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();

    // ユーザ一覧から管理者情報を外す(is_adminがtrueのものを外す)
    User adminUser = null;
    for (User user : userList) {
      if (user.isAdmin()) {
        adminUser = user;
      }
    }
    userList.remove(adminUser);

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);


    // ユーザ一覧のjspにフォワード（管理者かどうかによって表示が変わる）
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("loginId");
    String name = request.getParameter("name");
    String startBirthDate = request.getParameter("startBirthDate");
    String endBirthDate = request.getParameter("endBirthDate");

    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findUser(loginId, name, startBirthDate, endBirthDate);

    // ユーザ一覧から管理者情報を外す(is_adminがtrueのものを外す)
    User adminUser = null;
    for (User user : userList) {
      if (user.isAdmin()) {
        adminUser = user;
      }
    }
    userList.remove(adminUser);

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // リクエストスコープに入力した検索条件をセット
    request.setAttribute("loginId", loginId);
    request.setAttribute("name", name);
    request.setAttribute("startBirthDate", startBirthDate);
    request.setAttribute("endBirthDate", endBirthDate);

    // ユーザ一覧jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

}
