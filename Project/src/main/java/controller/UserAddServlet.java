package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // TODO：ログインセッションがない場合、ログイン画面にリダイレクトさせる
    // セッションからログインユーザの情報をセット
    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {
      // ログインサーブレットへリダイレクト
      response.sendRedirect("LoginServlet");
      return;
    }

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);
  }


  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("loginid");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");

    // 登録日時を取得
    // 日付をデータベースに書き込む際の形式にかえる
    Date date = new Date();
    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dateStr = f.format(date);

    // ユーザ一全情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();


    // ①ログインIDが既に登録されていないか確認（登録されている場合、エラー表示）
    for (User user : userList) {
      if (loginId.equals(user.getLoginId())) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("existID", "既にこのログインIDは登録されています。");
      }
    }

    // ②入力確認（未入力の場合、エラー表示）
    if (loginId.isEmpty() || password.isEmpty() || passwordConfirm.isEmpty() || name.isEmpty()
        || birthDate.isEmpty()) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("notFill", "未入力の項目があります。");
    }

    // ③パスワードが一致しているかどうか確認（一致していない場合、エラー表示）
    if (!password.equals(passwordConfirm) && !password.isEmpty() && !passwordConfirm.isEmpty()) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("notMatch", "パスワードが一致していません。");
    }


    // エラー表示が一つでもある場合はフォワード
    if (!(request.getAttribute("existID") == null) || !(request.getAttribute("notFill") == null)
        || !(request.getAttribute("notMatch") == null)) {
      // 入力したものをスコープにセット（パスワード以外）
      request.setAttribute("loginId", loginId);
      request.setAttribute("name", name);
      request.setAttribute("birthDate", birthDate);

      // 新規作成jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }


    // パスワードを暗号化
    String passStr = PasswordEncorder.encordPassword(password);


    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行（新規ユーザー作成）
    userDao.createUser(loginId, name, birthDate, passStr, dateStr, dateStr);


    // ユーザ一生成後、そのユーザーでログインする
    User user = userDao.findByLoginInfo(loginId, passStr);

    /** テーブルに該当のデータが見つからなかった場合 * */
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ユーザー登録に失敗しました。");

      // 新規作成jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /** テーブルに該当のデータが見つかった場合 * */
    // セッションにユーザの情報をセット
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");
  }

}
