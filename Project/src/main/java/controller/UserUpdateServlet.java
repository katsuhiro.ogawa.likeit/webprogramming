package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }


  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // TODO：ログインセッションがない場合、ログイン画面にリダイレクトさせる
    // セッションからログインユーザの情報をセット
    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {
      // ログインサーブレットへリダイレクト
      response.sendRedirect("LoginServlet");
      return;
    }


    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータからidを入手
    int id = Integer.parseInt(request.getParameter("id"));

    // リンクで渡されたidと同じユーザ一の情報を取得
    UserDao userDao = new UserDao();
    User user = userDao.findById(id);

    // リクエストパラメータの初期値に該当ユーザ情報を入れる
    request.setAttribute("loginId", user.getLoginId());
    request.setAttribute("name", user.getName());
    request.setAttribute("birthDate", user.getBirthDate().toString());


    // 編集(更新)jspへフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
  }


  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");


    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("loginId");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");
    String name = request.getParameter("name");
    String birthDate = request.getParameter("birth-date");


    // ①入力確認（未入力の場合、エラー表示）
    if (name.isEmpty() || birthDate.isEmpty()) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("notFill", "未入力の項目があります。");
    }

    // ②パスワードが一致しているかどうか確認（一致していない場合、エラー表示）
    // if (!password.equals(passwordConfirm) && !password.isEmpty() && !passwordConfirm.isEmpty()) {
    if (!password.equals(passwordConfirm)) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("notMatch", "パスワードが一致していません。");
    }


    // エラー表示が一つでもある場合はフォワード
    if (!(request.getAttribute("notFill") == null) || !(request.getAttribute("notMatch") == null)) {

      // 入力したもの（パスワード除く）をスコープにセット
      request.setAttribute("loginId", loginId);
      request.setAttribute("name", name);
      request.setAttribute("birthDate", birthDate);

      // 編集(更新)jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;
    }


    String passStr = null;
    // パスワードを暗号化
    if (!password.isEmpty()) {
      passStr = PasswordEncorder.encordPassword(password);
    }

    // ユーザー編集実行
    UserDao userDao = new UserDao();
    userDao.updateUser(name, birthDate, passStr, loginId);


    // ユーザ一覧サーブレットにリダイレクト
    response.sendRedirect("UserListServlet");
  }
}
